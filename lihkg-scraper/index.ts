import {getPopularPostIds, savePopularPostIds} from "./src/anlyaser";
import {scrapPost} from "./src/postScrapper"
import {collectTrainingData} from "./src/collector";

const {findCategoryPosts} = require("./src/scrapper")
const {getHotPostIds} = require("./src/anlyaser")
const postIds = require("./data/postIds/postIds.json")

// run().catch(err => console.log(err))

// savePopularPostIds().catch(err => console.log(err))

export async function asyncForEach<T>(array: Array<T>, callback: (item: T, index: number) => void) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index);
  }
}

// let run = () => getPopularPostIds().then(
//     async arr => {
//       return await asyncForEach(arr, scrapPost)
//     }
// )
//
// run().catch(err => console.log(err)).then(run)


// getPopularPostIds().then(arr => arr.forEach(scrapPost))

// asyncForEach().catch(err => console.log(err))
//
// scrapPost("1977487").catch(err => console.log(err))

collectTrainingData().catch(err => console.log(err))