import * as fs from "fs";
import {promises as fsPromise} from "fs"
import {unparse} from "papaparse";
import * as jsdom from "jsdom"

export async function collectTrainingData() {
  let data = []
  const postIds = await fsPromise.readdir("./data/post")
  for (const postId of postIds) {
    if (!((await fsPromise.stat(`./data/post/${postId}`)).isDirectory())) {
      continue
    }
    const filenames = await fsPromise.readdir(`./data/post/${postId}`)
    for (const fn of filenames) {
      if (!fn.includes(".json")) {
        continue
      }
      const content = JSON.parse(
          (await fsPromise.readFile(
              `./data/post/${postId}/${fn}`,
              {encoding: "utf8"}
          )).toString()
      )
      const arr = content.response.item_data.map(
          ({thread_id, post_id, msg, like_count, dislike_count, no_of_quote}) => ({
            thread_id,
            post_id,
            no_of_quote,
            text: jsdom.JSDOM.fragment(msg).textContent.replace(/\s+/g, ' '),
            like_count,
            dislike_count,
            total_reaction_count: Math.abs(like_count) + Math.abs(dislike_count),
            score: Math.abs(like_count) - Math.abs(dislike_count),
            tag: Math.abs(like_count) + Math.abs(dislike_count) > 20 && Math.abs(like_count) / Math.abs(dislike_count) > 2 ?
                "good" : Math.abs(like_count) + Math.abs(dislike_count) > 20 && Math.abs(like_count) / Math.abs(dislike_count) <= 0.5 ? "bad" : null
          })
      ).filter(obj => !!obj.tag && !!obj.text)
      data = [...data, ...arr]
    }
  }
  const csv = unparse(data)
  await fsPromise.writeFile("./data/text_data.csv", csv, {encoding: "utf8"})
}