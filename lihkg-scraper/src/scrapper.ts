import * as puppeteer from 'puppeteer';
import {promises as fs} from "fs";

async function findCategoryPosts() {
  const browser = await puppeteer.launch({
    headless: false,
    devtools: true
  })

  const page = (await browser.pages())[0]

  await page.setCacheEnabled(false)

  // await page.setRequestInterception(true)

  await page.on("response", async response => {
    if (response.url().includes("api_v2/thread/category") && response.ok()) {
      const jsonData = await response.json()
      const spittedUrl = response.url().split("?")
      await fs.writeFile(`data/${spittedUrl[spittedUrl.length - 1]}.json`, JSON.stringify(jsonData), {encoding: "utf8"})
    }
  })

  // const navigationHandler = page.waitForNavigation({waitUntil: "domcontentloaded"})

  await page.goto("https://lihkg.com/category/5?order=now", {waitUntil: "networkidle0"})

  // await navigationHandler;

  for (; ;) {
    await page.waitFor(1000)
    const lastHandle = (await page.evaluateHandle(ele => {
      const children = document.querySelector("#leftPanel").children[1].children
      console.log(children[children.length - 2])
      return children[children.length - 2]
    })).asElement()
    const firstHandle = (await page.evaluateHandle(ele => {
      const children = document.querySelector("#leftPanel").children[1].children
      console.log(children[children.length - 2])
      return children[0]
    })).asElement()
    await lastHandle.hover()
    await page.waitFor(1000)
    await firstHandle.hover()
  }


}


export {findCategoryPosts}

