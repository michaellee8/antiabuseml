import * as fs from "fs";
import {promises as fsPromise} from "fs"

export async function getHotPostIds(): Promise<Array<string>> {
  const dir = await fsPromise.readdir("./data/cat")
  // console.log(dir)
  const postIds = dir.map(
      fn => {
        const content = fs.readFileSync(`./data/cat/${fn}`, {encoding: "utf8"}).toString()
        // console.log(content)
        return JSON.parse(content)
      }
  ).map(
      d => d.response
      .items
      .filter(
          item => item.is_hot
      )
  ).reduce(
      (arr1, arr2) => [...arr1, ...arr2], []
  ).map(item => item.thread_id)
  return postIds
}

export async function getPopularPostIds(): Promise<Array<string>> {
  const dir = await fsPromise.readdir("./data/cat")
  // console.log(dir)
  const postIds = dir.map(
      fn => {
        const content = fs.readFileSync(`./data/cat/${fn}`, {encoding: "utf8"}).toString()
        // console.log(content)
        return JSON.parse(content)
      }
  ).map(
      d => d.response
      .items
      .filter(
          item => item.like_count + Math.abs(item.dislike_count) >= 100
      )
  ).reduce(
      (arr1, arr2) => [...arr1, ...arr2], []
  ).map(item => item.thread_id)
  return postIds
}

export async function savePopularPostIds(): Promise<void> {
  const arr = await getPopularPostIds()
  await fsPromise.writeFile(
      "./data/postIds/postIds.json",
      JSON.stringify(arr),
      {encoding: "utf8"}
  )
}