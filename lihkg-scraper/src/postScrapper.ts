import * as puppeteer from 'puppeteer';
import {promises as fsPromise} from "fs";
import * as fs from "fs"

const postUrlRegex = /^https?:\/\/lihkg\.com\/api_v2\/thread\/(\d+)\/page\/(\d+)\?order=reply_time$/

const fileExists = async path => !!(await fs.promises.stat(path).catch(e => false));

export async function scrapPost(postId: string): Promise<void> {

  if (await fileExists(`data/post/${postId}/1.json`)) {
    console.log(`scrapped post ${postId} already, ignoring`)
    return
  }

  const browser = await puppeteer.launch({
    // devtools: true,
    // headless: false,
  })

  console.log(`scrapping post ${postId}`)

  const page = (await browser.pages())[0]

  await page.setCacheEnabled(false)

  await page.on("response", async response => {
    if (response.url().match(postUrlRegex) && response.ok()) {
      const [url, postId, page] = response.url().match(postUrlRegex)
      const jsonData = await response.json()
      await fsPromise.mkdir(`data/post/${postId}`, {recursive: true})
      await fsPromise.writeFile(
          `data/post/${postId}/${page}.json`,
          JSON.stringify(jsonData),
          {encoding: "utf8"}
      )
    }
  })

  await page.goto(`https://lihkg.com/thread/${postId}/page/1`, {waitUntil: "networkidle0"})

  for (let i = 0; i < 5; i++) {
    await page.waitFor(200)
    const lastHandle = (await page.evaluateHandle(ele => {
      const children = document.querySelector("#rightPanel").children
      return children[1]
    })).asElement()
    const firstHandle = (await page.evaluateHandle(ele => {
      const children = document.querySelector("#leftPanel").children[0].children
      return children[0]
    })).asElement()
    await lastHandle.hover()
    await page.waitFor(200)
    await firstHandle.hover()
  }

  await page.waitFor(200)

  await browser.close()
}

